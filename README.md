# PERSONAL NERD 3000

> RSS feed reader. Attempt for a job application. Task description [here](https://bitbucket.org/csgot/seohelis-devdocs/wiki/new-team-member/frontend/task1.md)
``` bash
npm install 
npm run dev
```

Browser should open. If not, open it manually and enter [http://localhost:8080] as address. Then enter an RSS feed url into input field and press Return or a "Read!" button. RSS feed will load. 
Feed history is recorded. NERD suggests you an URL from your history when atleast two symbols are entered and they match a pattern.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
