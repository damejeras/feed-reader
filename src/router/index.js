import Vue from 'vue'
import Router from 'vue-router'
import FeedReader from '@/components/FeedReader'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: FeedReader
    }
  ],
  mode: 'history'
})
